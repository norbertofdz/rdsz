/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.uc3m.it.webtlab.rdsz.compressors.rdsz;

import es.uc3m.it.webtlab.rdsz.utils.rdsz.Cache;
import es.uc3m.it.webtlab.rdsz.utils.rdsz.Node;
import es.uc3m.it.webtlab.rdsz.utils.rdsz.RDFItemDecomposition;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class RDSZDiffEncoder {
    
    int cacheSize;
    Cache<String, RDFItemDecomposition> cache;
    
    // LinkedList<String> patterns;
    
    public RDSZDiffEncoder(int cacheSize) {
        if (cacheSize <=0) {
            throw new RuntimeException("Invalid cache size");
        }
        this.cacheSize = cacheSize;
        this.cache = new Cache<>(cacheSize);
        
        // this.patterns = new LinkedList<String>();
    }
    
    
    public String encode(String item) {
        
        String encodedItem;
        
        // System.out.println(item);
        
        // Decompose the item in pattern and bindings
        RDFItemDecomposition diffModel = RDFItemDecomposition.buildFromRDFItem(item);
        String pattern = diffModel.getPattern();
        if (pattern.trim().length() == 0) {            
            throw new RuntimeException("Invalid pattern");
        }
        
        // Use differential encoding if the pattern is in the cache
        if (cache.containsKey(pattern)) {
            RDFItemDecomposition inCache = cache.get(pattern);
            encodedItem = serialize(diffModel, inCache);
        } else {            
            encodedItem = item;
        }                
        
        // System.out.println(encodedItem);
        
        // // Log the patterns 
        // if (!patterns.contains(pattern)) {
        //    System.out.println(pattern);
        //    patterns.add(pattern);
        // }
        
        // Update the cache
        cache.put(pattern, diffModel);                
        return encodedItem;
    }
    
    private String serialize(RDFItemDecomposition current, RDFItemDecomposition inCache) {
        
        StringBuilder result = new StringBuilder();
        
        // Append the identifier in the first line
        int idx = cache.getIdxForKey(inCache.getPattern());
        result.append(idx);
        result.append("\n");
        
        // Serialize the bindings
        LinkedHashMap<String, Node> bindingsInCache = inCache.getBindings();
        LinkedHashMap<String, Node> bindingsCurrent = current.getBindings();
        Set<Map.Entry<String, Node>> entries = bindingsCurrent.entrySet();
        for (Map.Entry<String, Node> entry : entries) {
            String key = entry.getKey();
            Node value = entry.getValue();
            if (value.equals(bindingsInCache.get(key))) {                
                result.append("\n");
            } else {
                switch(value.getType()) {
                    case BNODE:
                        result.append("_\n");
                        break;
                    case LITERAL:
                    case URI:
                        result.append(value.getText());
                        result.append("\n");                    
                        break;
                    default:
                        throw new RuntimeException("Unexpected node type");
                }              
            }
        }        
             
        return result.toString();
    }
}
