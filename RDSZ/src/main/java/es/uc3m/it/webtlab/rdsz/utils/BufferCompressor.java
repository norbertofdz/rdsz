/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class BufferCompressor {

    int level;
    int bufSize;
    Deflater compressor;
    byte[] buffer; 

    public BufferCompressor(int level) {
        this.level = level;
        this.compressor = new Deflater(level);
        this.bufSize = Constants.DEFAULT_BUFFER_SIZE;  
        this.buffer = new byte[this.bufSize];
    }

    public BufferCompressor(int level, int bufSize) {
        this.level = level;
        this.compressor = new Deflater(level);
        this.bufSize = bufSize;     
        this.buffer = new byte[this.bufSize];
    }

    public byte[] compress(byte[] data) throws IOException {

        // Configure deflater
        this.compressor.setInput(data, 0, data.length);

        // Compress and leave results in buffer
        byte[] output;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {            
            while (!this.compressor.needsInput()) {
                int size = this.compressor.deflate(buffer, 0, buffer.length);
                outputStream.write(buffer, 0, size);
            }

            // SYNC_FLUSH
            int size = this.compressor.deflate(buffer, 0, buffer.length, Deflater.SYNC_FLUSH);
            outputStream.write(buffer, 0, size);
            outputStream.flush();

            // Copy result to output
            output = outputStream.toByteArray();
        }
        
        return output;
    }

    public void close() {
        this.compressor.end();
    }
}
