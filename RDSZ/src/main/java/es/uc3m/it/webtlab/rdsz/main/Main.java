/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.main;

import es.uc3m.it.webtlab.rdsz.app.Application;
import es.uc3m.it.webtlab.rdsz.compressors.Compressor;
import es.uc3m.it.webtlab.rdsz.compressors.NullCompressor;
import es.uc3m.it.webtlab.rdsz.compressors.ZlibCompressor;
import es.uc3m.it.webtlab.rdsz.compressors.rdsz.RDSZCompressor;
import es.uc3m.it.webtlab.rdsz.decompressors.Decompressor;
import es.uc3m.it.webtlab.rdsz.decompressors.NullDecompressor;
import es.uc3m.it.webtlab.rdsz.decompressors.ZlibDecompressor;
import es.uc3m.it.webtlab.rdsz.decompressors.rdsz.RDSZDecompressor;
import es.uc3m.it.webtlab.rdsz.utils.Constants;
import es.uc3m.it.webtlab.rdsz.utils.Constants.Algos;
import es.uc3m.it.webtlab.rdsz.utils.Constants.Parsers;
import es.uc3m.it.webtlab.rdsz.utils.Constants.Serializations;
import es.uc3m.it.webtlab.rdsz.utils.ItemEvtFileParser;
import es.uc3m.it.webtlab.rdsz.utils.ItemFileParser;
import es.uc3m.it.webtlab.rdsz.utils.ItemTtlFileParser;
import es.uc3m.it.webtlab.rdsz.utils.Stats;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class Main {

    public static void main(String args[]) {

        try {

            // Configure Jena logging
            org.apache.log4j.Logger.getLogger("com.hp.hpl.jena.arq.info").setLevel(org.apache.log4j.Level.ERROR);
            org.apache.log4j.Logger.getLogger("com.hp.hpl.jena.arq.exec").setLevel(org.apache.log4j.Level.ERROR);
            org.apache.log4j.Logger.getLogger("org.apache.jena.riot").setLevel(org.apache.log4j.Level.ERROR);

            // Configure the options available
            Options options = new Options();
            Option algoOpt = new Option("a", "algo", true, "Compression algorithm");
            algoOpt.setRequired(true);
            options.addOption(algoOpt);

            Option fileOpt = new Option("f", "file", true, "File to process");
            fileOpt.setRequired(true);
            options.addOption(fileOpt);

            Option batchOpt = new Option("b", "batch", true, "Batch size");
            batchOpt.setRequired(false);
            options.addOption(batchOpt);

            Option iserialOpt = new Option("is", "iserial", true, "RDF serialization for input file");
            iserialOpt.setRequired(false);
            options.addOption(iserialOpt);

            Option cserialOpt = new Option("cs", "cserial", true, "RDF serialization to be used at compressor");
            cserialOpt.setRequired(false);
            options.addOption(cserialOpt);

            Option cacheOpt = new Option("c", "cache", true, "Size of the cache of items");
            cacheOpt.setRequired(false);
            options.addOption(cacheOpt);

            Option maxItemsOpt = new Option("i", "items", true, "Maximum number of RDF items to process");
            maxItemsOpt.setRequired(false);
            options.addOption(maxItemsOpt);

            Option levelOpt = new Option("l", "level", true, "Compression level");
            levelOpt.setRequired(false);
            options.addOption(levelOpt);

            Option varOpt = new Option("v", "var", false, "Use variable batch size");
            varOpt.setRequired(false);
            options.addOption(varOpt);

            Option parserOpt = new Option("p", "parser", true, "Parser to process input file");
            parserOpt.setRequired(true);
            options.addOption(parserOpt);

            Option memBufOpt = new Option("m", "mem", true, "Memory to be allocated for compression buffer (KBs)");
            memBufOpt.setRequired(false);
            options.addOption(memBufOpt);

            Option helpOpt = new Option("h", "help", false, "Shows help");
            helpOpt.setRequired(false);
            options.addOption(helpOpt);

            // Parse input arguments
            CommandLineParser cliParser = new BasicParser();
            CommandLine cmdLine = cliParser.parse(options, args);

            // Read the arguments
            Algos algo = Constants.DEFAULT_ALGO;
            if (cmdLine.hasOption("a")) {
                algo = Algos.valueOf(cmdLine.getOptionValue("a").toUpperCase());
            }
            if (cmdLine.hasOption("algo")) {
                algo = Algos.valueOf(cmdLine.getOptionValue("algo").toUpperCase());
            }

            String fileName = null;
            if (cmdLine.hasOption("f")) {
                fileName = cmdLine.getOptionValue("f");
            }
            if (cmdLine.hasOption("file")) {
                fileName = cmdLine.getOptionValue("file");
            }

            int batchSize = Constants.DEFAULT_BATCH_SIZE;
            if (cmdLine.hasOption("b")) {
                batchSize = Integer.parseInt(cmdLine.getOptionValue("b"));
            }
            if (cmdLine.hasOption("batch")) {
                batchSize = Integer.parseInt(cmdLine.getOptionValue("batch"));
            }

            int cacheSize = Constants.DEFAULT_CACHE_SIZE;
            if (cmdLine.hasOption("c")) {
                cacheSize = Integer.parseInt(cmdLine.getOptionValue("c"));
            }
            if (cmdLine.hasOption("cache")) {
                cacheSize = Integer.parseInt(cmdLine.getOptionValue("cache"));
            }

            int maxItems = Constants.ALL_ITEMS;
            if (cmdLine.hasOption("i")) {
                maxItems = Integer.parseInt(cmdLine.getOptionValue("i"));
            }
            if (cmdLine.hasOption("items")) {
                maxItems = Integer.parseInt(cmdLine.getOptionValue("items"));
            }

            int level = Constants.DEFAULT_COMPRESSION_LEVEL;
            if (cmdLine.hasOption("l")) {
                level = Integer.parseInt(cmdLine.getOptionValue("l"));
            }
            if (cmdLine.hasOption("level")) {
                level = Integer.parseInt(cmdLine.getOptionValue("level"));
            }

            boolean var = Constants.DEFAULT_VAR_BATCH;
            if (cmdLine.hasOption("v")) {
                var = true;
            }
            if (cmdLine.hasOption("var")) {
                var = true;
            }

            Serializations iserial = Serializations.TTL;
            if (cmdLine.hasOption("is")) {
                iserial = Serializations.valueOf(cmdLine.getOptionValue("is").toUpperCase());
            }
            if (cmdLine.hasOption("iserial")) {
                iserial = Serializations.valueOf(cmdLine.getOptionValue("iserial").toUpperCase());
            }

            Serializations cserial = Serializations.TTL;
            if (cmdLine.hasOption("cs")) {
                cserial = Serializations.valueOf(cmdLine.getOptionValue("cs").toUpperCase());
            }
            if (cmdLine.hasOption("cserial")) {
                cserial = Serializations.valueOf(cmdLine.getOptionValue("cserial").toUpperCase());
            }

            Parsers parserValue = Parsers.EVT;
            if (cmdLine.hasOption("p")) {
                parserValue = Parsers.valueOf(cmdLine.getOptionValue("p").toUpperCase());
            }
            if (cmdLine.hasOption("parser")) {
                parserValue = Parsers.valueOf(cmdLine.getOptionValue("parser").toUpperCase());
            }


            int bufSize = Constants.DEFAULT_BUFFER_SIZE;
            if (cmdLine.hasOption("m")) {
                bufSize = Integer.parseInt(cmdLine.getOptionValue("m"));
                bufSize *= 1024;
            }
            if (cmdLine.hasOption("mem")) {
                bufSize = Integer.parseInt(cmdLine.getOptionValue("mem"));
                bufSize *= 1024;
            }


            if (cmdLine.hasOption("h") || cmdLine.hasOption("help")) {
                HelpFormatter format = new HelpFormatter();
                format.printUsage(new PrintWriter(System.out), 80, "RDSZ", options);
            }

            // Build the compressor/decompressor
            Compressor compressor;
            Decompressor decompressor;
            switch (algo) {
                case NULL:
                    compressor = new NullCompressor(Constants.DEFAULT_DELIMITER);
                    decompressor = new NullDecompressor(Constants.DEFAULT_DELIMITER);
                    break;
                case RDSZ:
                    compressor = new RDSZCompressor(Constants.DEFAULT_DELIMITER, level, cacheSize, bufSize);
                    decompressor = new RDSZDecompressor(Constants.DEFAULT_DELIMITER, level, cacheSize, bufSize);
                    // RDSZ uses turtle internally (overide cs flag)
                    cserial = Serializations.TTL;
                    break;
                case ZLIB:
                default:
                    compressor = new ZlibCompressor(Constants.DEFAULT_DELIMITER, level, bufSize);
                    decompressor = new ZlibDecompressor(Constants.DEFAULT_DELIMITER, level, bufSize);
            }

            // Build the RDF item file parser 
            ItemFileParser parser = null;
            switch (parserValue) {
                case EVT:
                    parser = new ItemEvtFileParser(new File(fileName));
                    break;
                case TTL:
                    parser = new ItemTtlFileParser(new File(fileName));
                    break;
                default:
                    throw new RuntimeException("Invalid parser type");
            }

            // Configure the application and run it
            Application app = new Application();
            app.setBatchSize(batchSize);
            app.setFileName(fileName);
            app.setMaxItems(maxItems);
            app.setVariable(var);
            app.setCompressor(compressor);
            app.setDecompressor(decompressor);
            app.setInputFileSerialization(iserial);
            app.setCompressorSerialization(cserial);
            app.setItemFileParser(parser);

            // Output logging information
            Stats stats = app.run();
            StringBuilder builder = new StringBuilder();
            builder.append(new File(fileName).getName());
            builder.append("\t");
            builder.append(algo);
            builder.append("\t");
            builder.append(cserial);
            builder.append("\t");
            builder.append(batchSize);
            builder.append("\t");
            builder.append(cacheSize);
            builder.append("\t");
            if (var) {
                builder.append("VAR");
            } else {
                builder.append("FIX");
            }
            builder.append("\t");
            builder.append(stats);
            builder.append("\t");

            System.out.println(builder.toString());

        } catch (ParseException | IOException | DataFormatException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
