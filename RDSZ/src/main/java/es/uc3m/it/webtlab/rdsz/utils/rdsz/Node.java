/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils.rdsz;

import es.uc3m.it.webtlab.rdsz.utils.Constants.NodeTypes;
import java.util.Objects;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class Node implements Comparable {

    String text;
    NodeTypes type;       
    
    public Node(String text, NodeTypes type) {
        this.text = text;
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public NodeTypes getType() {
        return type;
    }
        

    @Override
    public int compareTo(Object o) {
        Node other = (Node) o;
        return this.text.compareTo(other.text);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.text);
        hash = 53 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Node other = (Node) obj;
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        return this.type == other.type;
    }
           
    @Override
    public String toString() {
        return text;
    }
}
