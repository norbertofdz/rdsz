/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.decompressors.rdsz;

import es.uc3m.it.webtlab.rdsz.utils.Constants.NodeTypes;
import es.uc3m.it.webtlab.rdsz.utils.rdsz.Cache;
import es.uc3m.it.webtlab.rdsz.utils.rdsz.Node;
import es.uc3m.it.webtlab.rdsz.utils.rdsz.RDFItemDecomposition;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class RDSZDiffDecoder {

    int cacheSize;
    Cache<String, RDFItemDecomposition> cache;

    public RDSZDiffDecoder(int cacheSize) {
        if (cacheSize <= 0) {
            throw new RuntimeException("Invalid cache size");
        }
        this.cacheSize = cacheSize;
        this.cache = new Cache<>(cacheSize);
    }

    public String decode(String item) {

        String decoded;

        // The item is encoded if the first line is an integer
        RDFItemDecomposition current;
        
        // Divide by EOL
        int pos = 0, end;
        List<String> lines = new ArrayList<>();         
        while ((end = item.indexOf('\n', pos)) >= 0) {            
            lines.add(item.substring(pos, end));
            pos = end + 1;
        }

        // There are no EOLs (single line item)
        if (lines.isEmpty()) {
            lines.add(item);
        }        
        
        try {
            int idx = Integer.parseInt(lines.get(0));            
            RDFItemDecomposition inCache = cache.get(idx);
            current = deserialize(lines, inCache);
            decoded = current.toRDFItem();

        } catch (NumberFormatException e) {
            current = RDFItemDecomposition.buildFromRDFItem(item);
            decoded = item;
        }

        // System.out.println(decoded);
        // Update cache        
        cache.put(current.getPattern(), current);
        return decoded;
    }

    public RDFItemDecomposition deserialize(List<String> lines, RDFItemDecomposition inCache) {

        int varIdx = 0;
        LinkedHashMap<String, Node> inCacheBindings = inCache.getBindings();
        LinkedHashMap<String, Node> currentBindings = new LinkedHashMap<>();
        for (int k = 1; k < lines.size(); k++) {
            String var = "_:" + varIdx;
            String line = lines.get(k);
            // Empty line, same value as in cache bindings
            Node node;
            if (line.isEmpty()) {
                node = inCacheBindings.get(var);
            } else {
                switch (line.charAt(0)) {
                    case '<':
                        node = new Node(line, NodeTypes.URI);
                        break;

                    case '_':
                        node = new Node(var, NodeTypes.BNODE);
                        break;
                    case '"':
                    default:
                        node = new Node(line, NodeTypes.LITERAL);
                }
            }
            currentBindings.put(var, node);
            varIdx++;
        }

        return new RDFItemDecomposition(inCache.getPattern(), currentBindings);
    }       
}
