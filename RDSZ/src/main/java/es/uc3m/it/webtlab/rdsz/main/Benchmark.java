/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.main;

import java.io.File;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class Benchmark {

    private static final String[] files = {
        "identica.evt",
        "mix.evt",
        "wikipedia.evt",
        "aemet-1.evt",
        "aemet-2.evt",
        "petrol.evt",
        "LOD.evt"
    };

    public static void main(String args[]) {

        String path = "";
        String fileWithPath;
        String parserType;
        String BSIZE = Integer.toString(32);

        if (args.length > 0) {
            path = args[0];
            if (!path.endsWith(File.separator)) {
                path = path + File.separator;
            }
        }

        for (String file : files) {
            fileWithPath = path + file;
            if (fileWithPath.endsWith("evt")) {
                parserType = "evt";
            } else if (fileWithPath.endsWith("ttl")) {
                parserType = "ttl";
            } else {
                throw new RuntimeException("Unsupported file format");
            }

            // Run RDSZ on a set of files using a cache size of 100
            String[] params = {"-b", "5", "-m", BSIZE, "-p", parserType, "-a", "rdsz", "-cs", "ttl", "-l", "9", "-f", fileWithPath, "-c", "100"};
            Main.main(params);
        }
    }
}
