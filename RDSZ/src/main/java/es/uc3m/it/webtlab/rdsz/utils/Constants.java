/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.uc3m.it.webtlab.rdsz.utils;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class Constants {
 
    public static enum Parsers {
        EVT, TTL
    }
    
    public static enum NodeTypes {
        URI, BNODE, LITERAL
    }
    
    public static enum Algos {
        NULL, ZLIB, RDSZ
    }
    
    public static enum Serializations {
        XML, TTL, NT, LD, THRIFT
    }
    
    public static final int DEFAULT_BATCH_SIZE = 5;
    public static final int DEFAULT_BUFFER_SIZE = 32*1024;
    public static final int DEFAULT_CACHE_SIZE = 128;
    public static final Algos DEFAULT_ALGO = Algos.ZLIB;
    public static final int DEFAULT_COMPRESSION_LEVEL = 9;
    public static final boolean DEFAULT_VAR_BATCH = false;
    public static final String DEFAULT_DELIMITER = "\nE0E\n";
    
    public static final int ALL_ITEMS = -1;
}
