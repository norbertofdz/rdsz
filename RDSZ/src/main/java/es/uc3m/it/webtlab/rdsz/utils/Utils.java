/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import es.uc3m.it.webtlab.rdsz.utils.Constants.Serializations;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.io.StringWriter;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class Utils {

    public static Model stringToModel(String item, Serializations serialization) {
        StringReader reader = new StringReader(item);
        Model model = ModelFactory.createDefaultModel();
        RDFDataMgr.read(model, reader, null, serialToJenaLang(serialization));
        return model;
    }

    public static String modelToString(Model model, Serializations serialization) {
        StringWriter writer = new StringWriter();
        RDFDataMgr.write(writer, model, serialToJenaLang(serialization));
        return writer.toString();
    }

    public static Model binToModel(byte[] item, Serializations serialization) {
        BufferedInputStream input = new BufferedInputStream(new ByteArrayInputStream(item));
        Model model = ModelFactory.createDefaultModel();
        RDFDataMgr.read(model, input, null, serialToJenaLang(serialization));
        return model;
    }

    public static byte[] modelToBin(Model model, Serializations serialization) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        RDFDataMgr.write(output, model, serialToJenaLang(serialization));
        return output.toByteArray();
    }

    public static Lang serialToJenaLang(Serializations serial) {
        switch (serial) {
            case XML:
                return Lang.RDFXML;
            case NT:
                return Lang.NTRIPLES;
            case LD:
                return Lang.JSONLD;
            case THRIFT:
                return Lang.RDFTHRIFT;
            case TTL:
            default:
                return Lang.TURTLE;
        }
    }

    public static ItemFileParser getItemFileParserForFileName(String file) throws FileNotFoundException {
        ItemFileParser parser;
        if (file.endsWith("evt")) {
            parser = new ItemEvtFileParser(new File(file));
        } else if (file.endsWith("ttl")) {
            parser = new ItemTtlFileParser(new File(file));
        } else {
            throw new RuntimeException("Unsupported file format");
        }
        return parser;
    }

    public static boolean itemIsWellFormed(String item) {

        // The item should have body (last line should not be a prefix declaration)
        String[] lines = item.split("\n");
        return !lines[lines.length - 1].startsWith("@prefix");
    }
    

    public static String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
