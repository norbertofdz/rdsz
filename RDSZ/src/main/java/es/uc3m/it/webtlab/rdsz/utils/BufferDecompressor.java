/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class BufferDecompressor {

    int bufSize;
    Inflater decompressor;
    byte[] buffer; 

    public BufferDecompressor() {
        this.decompressor = new Inflater();
        this.bufSize = Constants.DEFAULT_BUFFER_SIZE;
        this.buffer = new byte[this.bufSize];
    }

    public BufferDecompressor(int bufSize) {
        this.decompressor = new Inflater();
        this.bufSize = bufSize;
        this.buffer = new byte[this.bufSize];
    }

    public byte[] decompress(byte[] data) throws IOException, DataFormatException {

        // Configure inflater
        decompressor.setInput(data, 0, data.length);

        // Decompress and leave results in buffer
        byte[] output;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {            
            while (!decompressor.needsInput()) {
                int size = decompressor.inflate(buffer, 0, buffer.length);
                outputStream.write(buffer, 0, size);
            }
            outputStream.flush();

            // Copy result to output
            output = outputStream.toByteArray();
        }

        return output;
    }

    public void close() {
        decompressor.end();
    }
}
