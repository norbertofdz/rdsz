/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils.rdsz;

import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import es.uc3m.it.webtlab.rdsz.utils.Constants;
import es.uc3m.it.webtlab.rdsz.utils.Constants.NodeTypes;
import es.uc3m.it.webtlab.rdsz.utils.Utils;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.lang.PipedRDFIterator;
import org.apache.jena.riot.lang.PipedTriplesStream;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class RDFItemDecomposition {

    private static final Model m = ModelFactory.createDefaultModel();
    private static final N3Formatter formatter = N3Formatter.getInstance();

    String pattern;
    LinkedHashMap<String, Node> bindings;

    /**
     * Private constructor. Use static method buildFromModel instead.
     */
    private RDFItemDecomposition() {
    }

    public RDFItemDecomposition(String pattern, LinkedHashMap<String, Node> bindings) {
        this.pattern = pattern;
        this.bindings = bindings;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public LinkedHashMap<String, Node> getBindings() {
        return bindings;
    }

    public void setBindings(LinkedHashMap<String, Node> bindings) {
        this.bindings = bindings;
    }

    public String toRDFItem() {

        StringBuilder rdfItem = new StringBuilder();
        // Parse the pattern line by line
        String[] lines = pattern.split("\n");
        
        for (String line : lines) {            
            String[] tokens = line.split(" ");
            Node subjectNode = bindings.get(tokens[0]);
            Node objectNode = bindings.get(tokens[2]);

            // Replace subject variable by value
            if (subjectNode.type == NodeTypes.BNODE) {
                rdfItem.append(tokens[0]);
            } else {
                rdfItem.append(subjectNode);
            }

            // Append the predicate
            rdfItem.append(" ");
            rdfItem.append(tokens[1]);
            rdfItem.append(" ");

            // Replace object variable by value
            if (objectNode.type == NodeTypes.BNODE) {
                rdfItem.append(tokens[2]);
            } else {
                rdfItem.append(objectNode);
            }

            rdfItem.append(" .\n");
        }

        return rdfItem.toString();
    }

    public static RDFItemDecomposition buildFromRDFItem(String item) {

        int varIdx = 0;
        // Maps variables to varIds
        LinkedHashMap<Node, String> inverseBindings = new LinkedHashMap<>();
        // Maps varIds to variables
        LinkedHashMap<String, Node> tempBindings = new LinkedHashMap<>();
        // The triplePattern
        StringBuilder patternBuilder = new StringBuilder();

        HashMap<Node, HashMap<Node, List<Node>>> tree = buildTreeFromRDFItem(item);
        List<Node> subjects = new ArrayList<>(tree.keySet());
        Collections.sort(subjects);
                
        for (Node subject : subjects) {            
            String sId;
            if (inverseBindings.containsKey(subject)) {
                sId = inverseBindings.get(subject);
            } else {
                sId = "_:" + varIdx;
                inverseBindings.put(subject, sId);
                tempBindings.put(sId, subject);
                varIdx++;
            }

            HashMap<Node, List<Node>> subjectTree = tree.get(subject);
            List<Node> predicates = new ArrayList<>(subjectTree.keySet());
            Collections.sort(predicates);
                       
            for (Node predicate : predicates) {                
                List<Node> objects = subjectTree.get(predicate);
                // The objects in the tree are not sorted, sort them now
                Collections.sort(objects);
                for (Node object : objects) {
                    String oId;
                    if (inverseBindings.containsKey(object)) {
                        oId = inverseBindings.get(object);
                    } else {
                        oId = "_:" + varIdx;
                        inverseBindings.put(object, oId);
                        tempBindings.put(oId, object);
                        varIdx++;
                    }

                    patternBuilder.append(sId);
                    patternBuilder.append(" ");
                    patternBuilder.append(predicate);
                    patternBuilder.append(" ");
                    patternBuilder.append(oId);
                    patternBuilder.append(" .\n");
                }
            }
        }

        RDFItemDecomposition result = new RDFItemDecomposition();
        result.setPattern(patternBuilder.toString());
        result.setBindings(tempBindings);
        return result;
    }

    private static HashMap<Node, HashMap<Node, List<Node>>> buildTreeFromRDFItem(String item) {

        int bnodeIdx = 0;

        HashMap<String, Integer> bnodeTable = new HashMap<>();
        HashMap<Node, HashMap<Node, List<Node>>> results = new HashMap<>();
        
        StringReader reader = new StringReader(item);
              
        // Call the parsing process
        PipedRDFIterator<Triple> iter = new PipedRDFIterator<>(4 * item.length());
        PipedTriplesStream inputStream = new PipedTriplesStream(iter);
        RDFDataMgr.parse(inputStream, reader, null, Utils.serialToJenaLang(Constants.Serializations.TTL));
        
        // Process triples one by one
        while (iter.hasNext()) {

            Triple triple = iter.next();

            // Process subject
            com.hp.hpl.jena.graph.Node s = triple.getSubject();

            Node subjectNode;
            if (s.isBlank()) {
                int idx;
                String sStr = s.getBlankNodeLabel();
                if (bnodeTable.containsKey(sStr)) {
                    idx = bnodeTable.get(sStr);
                } else {
                    idx = bnodeIdx;
                    bnodeTable.put(sStr, idx);
                    bnodeIdx++;
                }

                subjectNode = new Node("_:" + idx, NodeTypes.BNODE);

            } else if (s.isURI()) {
                subjectNode = new Node(formatter.formatURI(s.getURI()), NodeTypes.URI);
            } else {
                throw new RuntimeException("Unexpected node type for subject");
            }

            // Process predicate
            com.hp.hpl.jena.graph.Node p = triple.getPredicate();

            HashMap<Node, List<Node>> subjectTree;
            if (!results.containsKey(subjectNode)) {
                subjectTree = new HashMap<>();
                results.put(subjectNode, subjectTree);
            } else {
                subjectTree = results.get(subjectNode);
            }

            Node predicateNode = new Node(formatter.formatURI(p.getURI()), NodeTypes.URI);
            List<Node> objectsList;
            if (!subjectTree.containsKey(predicateNode)) {
                objectsList = new LinkedList<>();
                subjectTree.put(predicateNode, objectsList);
            } else {
                objectsList = subjectTree.get(predicateNode);
            }

            // Process object            
            com.hp.hpl.jena.graph.Node o = triple.getObject();

            Node objectNode;
            if (o.isBlank()) {
                int idx;
                String oStr = o.getBlankNodeLabel();
                if (bnodeTable.containsKey(oStr)) {
                    idx = bnodeTable.get(oStr);
                } else {
                    idx = bnodeIdx;
                    bnodeTable.put(oStr, idx);
                    bnodeIdx++;
                }

                objectNode = new Node("_:" + idx, NodeTypes.BNODE);

            } else if (o.isURI()) {
                objectNode = new Node(formatter.formatURI(o.getURI()), NodeTypes.URI);
            } else if (o.isLiteral()) {
                Literal l = (Literal) m.asRDFNode(o);
                objectNode = new Node(formatter.formatLiteral(l), NodeTypes.LITERAL);
            } else {
                throw new RuntimeException("Unexpected node type for object");
            }

            objectsList.add(objectNode);
        }

        return results;
    }

/*    
    public static RDFItemDecomposition buildFromRDFItem(String item) {

        int varIdx = 0;
        HashMap<String, Integer> bnodeTable = new HashMap<>();

        // Maps variables to varIds
        LinkedHashMap<Node, String> inverseBindings = new LinkedHashMap<>();
        // Maps varIds to variables
        LinkedHashMap<String, Node> tempBindings = new LinkedHashMap<>();
        // The triplePattern
        StringBuilder patternBuilder = new StringBuilder();

        // Call the parsing process
        StringReader reader = new StringReader(item);
        PipedRDFIterator<Triple> iter = new PipedRDFIterator<>(Constants.DEFAULT_BUFFER_SIZE);
        PipedTriplesStream inputStream = new PipedTriplesStream(iter);
        RDFDataMgr.parse(inputStream, reader, null, Utils.serialToJenaLang(Constants.Serializations.TTL));

        // Process triples one by one
        List<ItemTriple> triples = new LinkedList<>();
        while (iter.hasNext()) {
            Triple trp = iter.next();
            ItemTriple triple = new ItemTriple(trp, bnodeTable);
            triples.add(triple);
        }

        // Order the triples
        Collections.sort(triples);

        for (ItemTriple triple : triples) {

            Node subject = triple.getSubject();
            String sId;
            if (inverseBindings.containsKey(subject)) {
                sId = inverseBindings.get(subject);
            } else {
                sId = "_:" + varIdx;
                inverseBindings.put(subject, sId);
                tempBindings.put(sId, subject);
                varIdx++;
            }

            Node predicate = triple.getPredicate();
            Node object = triple.getObject();
            String oId;
            if (inverseBindings.containsKey(object)) {
                oId = inverseBindings.get(object);
            } else {
                oId = "_:" + varIdx;
                inverseBindings.put(object, oId);
                tempBindings.put(oId, object);
                varIdx++;
            }

            patternBuilder.append(sId);
            patternBuilder.append(" ");
            patternBuilder.append(predicate);
            patternBuilder.append(" ");
            patternBuilder.append(oId);
            patternBuilder.append(" .\n");
        }

        RDFItemDecomposition result = new RDFItemDecomposition();
        result.setPattern(patternBuilder.toString());
        result.setBindings(tempBindings);
        return result;
    }
*/    
}
