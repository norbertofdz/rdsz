/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.decompressors;

import es.uc3m.it.webtlab.rdsz.utils.BufferDecompressor;
import es.uc3m.it.webtlab.rdsz.utils.Constants.Serializations;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.DataFormatException;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class ZlibDecompressor implements Decompressor {

    int level;
    String delimiter;
    Serializations serialization;
    BufferDecompressor decompressor;
    

    public ZlibDecompressor(String delimiter, int level, int bufSize) {
        this.delimiter = delimiter;
        this.decompressor = new BufferDecompressor(bufSize);
        this.level = level;
    }

    @Override
    public List<String> decompress(byte[] data) throws IOException, DataFormatException {

        byte[] output = decompressor.decompress(data);
        String itemsStr = new String(output, "UTF-8");
        String[] items = itemsStr.split(this.delimiter);
        return Arrays.asList(items);
    }

}
