/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class Stats {

    long compressedSize;
    long sentItems;
    long sentTriples;
    long compressionTimeMillis;
    long decompressionTimeMillis;
    boolean errors;

    public long getCompressedSize() {
        return compressedSize;
    }

    public void setCompressedSize(long compressedSize) {
        this.compressedSize = compressedSize;
    }

    public long getSentItems() {
        return sentItems;
    }

    public void setSentItems(long sentItems) {
        this.sentItems = sentItems;
    }

    public long getSentTriples() {
        return sentTriples;
    }

    public void setSentTriples(long sentTriples) {
        this.sentTriples = sentTriples;
    }    
    
    public long getCompressionTimeMillis() {
        return compressionTimeMillis;
    }

    public void setCompressionTimeMillis(long compressionTimeMillis) {
        this.compressionTimeMillis = compressionTimeMillis;
    }

    public long getDecompressionTimeMillis() {
        return decompressionTimeMillis;
    }

    public void setDecompressionTimeMillis(long decompressionTimeMillis) {
        this.decompressionTimeMillis = decompressionTimeMillis;
    }

    public boolean isErrors() {
        return errors;
    }

    public void setErrors(boolean errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append(this.sentItems);
        builder.append("\t");
        builder.append(this.sentTriples);
        builder.append("\t");
        if (this.errors) {
            builder.append("ERROR");
        } else {
            builder.append("OK");
        }
        builder.append("\t");
        builder.append(this.compressedSize);
        builder.append("\t");
        builder.append(this.compressionTimeMillis);
        builder.append("\t");
        builder.append(this.decompressionTimeMillis);
        builder.append("\t");
        return builder.toString();
    }
}
