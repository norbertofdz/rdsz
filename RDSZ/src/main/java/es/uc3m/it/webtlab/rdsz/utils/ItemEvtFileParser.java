/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class ItemEvtFileParser implements ItemFileParser {

    String item;
    String previousLine;
    BufferedReader reader;
   
    
    public ItemEvtFileParser(File fileToRead) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(fileToRead));
    }

    
    @Override
    public String read() {

        StringBuilder builder = new StringBuilder();
        String line;
        try {

            // The first prefix line has already been read and needs to be appended
            if (this.previousLine != null) {
                builder.append(this.previousLine);
                builder.append("\n");
                this.previousLine = null;
            }

            // Append lines to item until EOF or next item prefix is found
            while ((line = this.reader.readLine()) != null) {

                if (line.startsWith("@prefix")
                        && (this.previousLine != null)
                        && (this.previousLine.trim().isEmpty())) {
                    this.previousLine = line;
                    break;
                }

                builder.append(line);
                builder.append("\n");
                this.previousLine = line;
            }

            // Check if it is an empty item or not
            String read = builder.toString();
            if (read.trim().isEmpty()) {
                return null;
            } else {
                this.item = read;
                return this.item.trim();
            }
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    public List<String> read(int n) {
        LinkedList<String> result = new LinkedList<>();
        int readed = 0;
        while (readed < n) {
            String readItem = read();
            if (readItem != null) {
                if (Utils.itemIsWellFormed(readItem)) {
                    result.add(readItem);
                    readed++;
                }
            } else {
                break;
            }
        }

        if (result.size() > 0) {
            return result;
        } else {
            return null;
        }
    }

 
    @Override
    public void close() {
        if (this.reader != null) {
            try {
                this.reader.close();
            } catch (IOException ex) {
            }
        }
    }
}
