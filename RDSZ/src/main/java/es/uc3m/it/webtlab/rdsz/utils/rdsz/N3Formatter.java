/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils.rdsz;

import com.hp.hpl.jena.n3.N3JenaWriterCommon;
import com.hp.hpl.jena.rdf.model.Literal;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class N3Formatter extends N3JenaWriterCommon {    
    
    private static final N3Formatter instance = new N3Formatter();
    
    private N3Formatter() {       
        super();
    }

    public static N3Formatter getInstance() {
        return instance;
    }
    
    @Override
    public String formatLiteral(Literal l) {
        return super.formatLiteral(l).replace("\n", "\\n");
    }   

    @Override
    public String formatURI(String uri) {
        StringBuilder builder = new StringBuilder();
        builder.append("<");
        builder.append(uri);
        builder.append(">");
        return builder.toString();        
    }
}
