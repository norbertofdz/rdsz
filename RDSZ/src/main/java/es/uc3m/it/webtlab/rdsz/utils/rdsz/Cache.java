/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils.rdsz;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * An LRU cache with fixed maximum size that maintains an index with a numeric
 * identifier associated to each entry.
 *
 * The entries in the cache can be accessed either by key or by identifier. The
 * identifier for a given entry can be obtained as result of the put method.
 *
 * Based on LRUCache code from Christian d'Heureuse, Inventec Informatik AG,
 * Zurich, Switzerland.
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class Cache<K, V> {

    private static final float hashTableLoadFactor = 0.75f;

    private K evicted;
    private int cacheSize;
    private final LinkedHashMap<K, V> map;
    private HashMap<Integer, K> index;
    private HashMap<K, Integer> inverseIndex;

    /**
     * Creates a new LRU cache.
     *
     * @param cacheSize the maximum number of entries that will be kept in this
     * cache.
     */
    public Cache(int cacheSize) {

        this.evicted = null;
        this.cacheSize = cacheSize;
        this.index = new HashMap<>(cacheSize);
        this.inverseIndex = new HashMap<>(cacheSize);

        int hashTableCapacity = (int) Math.ceil(cacheSize / hashTableLoadFactor) + 1;
        this.map = new LinkedHashMap<K, V>(hashTableCapacity, hashTableLoadFactor, true) {

            private static final long serialVersionUID = 1;

            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                boolean flag = (size() > Cache.this.cacheSize);
                if (flag) {
                    Cache.this.evicted = eldest.getKey();
                } else {
                    Cache.this.evicted = null;
                }                
                return flag;
            }
        };
    }

    /**
     * Obtain the key associated to a given identifier.
     *
     * @param idx The identifier
     * @return The key (or null, if the identifier is not included in the cache)
     */
    public K getKeyForIdx(int idx) {
        return this.index.get(idx);
    }

    /**
     * Obtain the identifier associated to a given key.
     *
     * @param key The key
     * @return The identifier (or null, if the key is not included in the cache)
     */
    public int getIdxForKey(K key) {
        return this.inverseIndex.get(key);
    }

    /**
     * Retrieves an entry from the cache.
     *
     * The retrieved entry becomes the MRU (most recently used) entry.
     *
     * @param key The key whose associated value is to be returned.
     * @return The value associated to this key, or null if no value with this
     * key exists in the cache.
     */
    public V get(K key) {
        return this.map.get(key);
    }

    /**
     * Retrieves an entry from the cache given the identifier.
     *
     * The retrieved entry becomes the MRU (most recently used) entry.
     *
     * @param idx The identifier whose associated value is to be returned.
     * @return The value associated to this key, or null if no value with this
     * identifier exists in the cache.
     */
    public V get(int idx) {
        if (this.index.containsKey(idx)) {
            K key = this.index.get(idx);
            return this.map.get(key);
        }
        return null;
    }
    
    /**
     * Check whether a particular key is in the map or not
     * 
     * @param key
     *  The key
     * @return
     *  A boolean
     */
    public boolean containsKey(K key) {
        return this.map.containsKey(key);
    }

    /**
     * Adds an entry to this cache.
     *
     * The new entry becomes the MRU (most recently used) entry. If an entry
     * with the specified key already exists in the cache, it is replaced by the
     * new entry. If the cache is full, the LRU (least recently used) entry is
     * removed from the cache.
     *
     * @param key The key with which the specified value is to be associated.
     * @param value A value to be associated with the specified key.
     * @return The identifier associated to the new inserted element.
     */
    public int put(K key, V value) {        
        boolean isNew = !this.map.containsKey(key);
        // Update the table
        this.map.put(key, value);
        // If an entry was evicted, update indexes
        int idx = usedEntries();
        if (this.evicted != null) {
            idx = this.inverseIndex.get(this.evicted);
            this.inverseIndex.remove(this.evicted);
            this.evicted = null;
        }
        // Insert the new entry in the indexes 
        if (isNew) {
            this.index.put(idx, key);
            this.inverseIndex.put(key, idx);
        }
        return idx;
    }

    /**
     * Clears the cache.
     */
    public void clear() {
        this.map.clear();
        this.index.clear();
        this.inverseIndex.clear();
    }

    /**
     * Returns the number of used entries in the cache.
     *
     * @return the number of entries currently in the cache.
     */
    public int usedEntries() {
        return this.map.size();
    }

    /**
     * Returns a <code>Collection</code> that contains a copy of all cache
     * entries.
     *
     * @return a <code>Collection</code> with a copy of the cache content.
     */
    public Collection<Map.Entry<K, V>> getAll() {
        return new ArrayList<>(this.map.entrySet());
    }

    @Override
    public String toString() {
        return this.map.toString();
    }
}
