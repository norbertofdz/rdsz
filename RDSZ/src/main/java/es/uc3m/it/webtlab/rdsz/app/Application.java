/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.app;

import com.hp.hpl.jena.rdf.model.Model;
import es.uc3m.it.webtlab.rdsz.compressors.Compressor;
import es.uc3m.it.webtlab.rdsz.decompressors.Decompressor;
import es.uc3m.it.webtlab.rdsz.utils.Constants.Serializations;
import es.uc3m.it.webtlab.rdsz.utils.ItemFileParser;
import es.uc3m.it.webtlab.rdsz.utils.Stats;
import es.uc3m.it.webtlab.rdsz.utils.Utils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.DataFormatException;
import org.apache.commons.math3.distribution.PoissonDistribution;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class Application {

    // Size of the batch of items between successive calls to flush
    int batchSize;

    // Maximum number of RDF items from the file to be processed
    int maxItems;

    // Should the app use variable batch size? (false means it is constant)
    boolean variable;

    // Input file with RDF items
    String fileName;

    // Compressor and decompressor to be used
    Compressor compressor;
    Decompressor decompressor;

    // The serialization used at the compressor 
    Serializations inputFileSerialization;
    Serializations compressorSerialization;

    // Parser for input file
    ItemFileParser parser; 
 
    
    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public void setMaxItems(int maxItems) {
        this.maxItems = maxItems;
    }

    public void setVariable(boolean variable) {
        this.variable = variable;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setCompressor(Compressor compressor) {
        this.compressor = compressor;
    }

    public void setDecompressor(Decompressor decompressor) {
        this.decompressor = decompressor;
    }

    public void setInputFileSerialization(Serializations inputFileSerialization) {
        this.inputFileSerialization = inputFileSerialization;
    }

    public void setCompressorSerialization(Serializations serialization) {
        this.compressorSerialization = serialization;
    }

    public void setItemFileParser(ItemFileParser parser) {
        this.parser = parser;
    }
    
        
    public Stats run() throws FileNotFoundException, IOException, DataFormatException {

        long numTriples = 0;
        long numItemsProcessed = 0;
        long totalBytesTransmitted = 0;
        long totalCompressionTime = 0;
        long totalDecompressionTime = 0;
        
        boolean errorFlag = false;

        int numToRead = batchSize;
        PoissonDistribution poisson = new PoissonDistribution(batchSize);
        
        do {

            // Variable batch size? Use poisson process
            if (variable) {
                do {
                    numToRead = poisson.sample();
                } while (numToRead == 0);
            }

            // Read the items            
            List<String> items = parser.read(numToRead);
            if (items == null) {
                break;
            }

            // Temporal storage for sent items (to compare them with the received ones)
            LinkedList<String> sentItems = new LinkedList<>();
            for (String item : items) {

                // System.out.println(item);
                if (inputFileSerialization != compressorSerialization) {
                    Model m = Utils.stringToModel(item, inputFileSerialization);
                    sentItems.add(Utils.modelToString(m, compressorSerialization));
                } else {
                    sentItems.add(item);
                }

                // Check if we have reached maxItems
                numItemsProcessed++;
                if (numItemsProcessed == maxItems) {
                    break;
                }
            }

            long begin = System.nanoTime();
            // Compress the items
            for (String item : sentItems) {
                compressor.compress(item);
            }

            byte[] toSend = compressor.flush();
            long elapsed = System.nanoTime() - begin;
            totalCompressionTime += elapsed;                      
            
            // The transmission line would go here ...
            totalBytesTransmitted += toSend.length;

            // Decompress the data received
            begin = System.nanoTime();
            List<String> receivedItems = decompressor.decompress(toSend);
            elapsed = System.nanoTime() - begin;
            totalDecompressionTime += elapsed;           

            // Compare the sent and received RDF items (should be isomorphic)
            int receivedSize = receivedItems.size();
            for (int k = 0; k < receivedSize; k++) {
                String sent = sentItems.get(k);
                String received = receivedItems.get(k);
                Model sentModel = Utils.stringToModel(sent, compressorSerialization);
                Model receivedModel = Utils.stringToModel(received, compressorSerialization);
                numTriples += sentModel.size();
                if (!receivedModel.isIsomorphicWith(sentModel)) {
                    errorFlag = true;                    
                }
            }
        } while (numItemsProcessed != maxItems);

        parser.close();

        Stats stats = new Stats();
        stats.setErrors(errorFlag);
        stats.setCompressedSize(totalBytesTransmitted);
        stats.setSentItems(numItemsProcessed);
        stats.setSentTriples(numTriples);
        stats.setCompressionTimeMillis(totalCompressionTime / 1000000);
        stats.setDecompressionTimeMillis(totalDecompressionTime / 1000000);

        return stats;
    }
}
