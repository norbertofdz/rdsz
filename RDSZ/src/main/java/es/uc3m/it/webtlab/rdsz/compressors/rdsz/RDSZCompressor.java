/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.uc3m.it.webtlab.rdsz.compressors.rdsz;

import es.uc3m.it.webtlab.rdsz.compressors.Compressor;
import es.uc3m.it.webtlab.rdsz.utils.BufferCompressor;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class RDSZCompressor implements Compressor {
    
    int level;
    int cacheSize;
    String delimiter;
    List<String> items;
    RDSZDiffEncoder encoder;
    BufferCompressor compressor;    
        
    public RDSZCompressor(String delimiter, int level, int cacheSize, int bufSize) {
        this.level = level;
        this.cacheSize = cacheSize;
        this.delimiter = delimiter;
        this.compressor = new BufferCompressor(level, bufSize);
        this.encoder = new RDSZDiffEncoder(cacheSize);        
        this.items = new LinkedList<>();
    }

    @Override
    public void compress(String rdfItem) {
        items.add(rdfItem);
    }
   
    @Override
    public byte[] flush() throws IOException {        
                
        StringBuilder builder = new StringBuilder("");
        for (String item : this.items) {
            builder.append(this.encoder.encode(item));
            builder.append(this.delimiter);
        }

        this.items.clear();                
                      
        byte[] inputBuffer = builder.toString().getBytes("UTF-8");
        return this.compressor.compress(inputBuffer);        
    }
}
