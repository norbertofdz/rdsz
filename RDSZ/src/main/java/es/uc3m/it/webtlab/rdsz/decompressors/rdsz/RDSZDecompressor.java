/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.uc3m.it.webtlab.rdsz.decompressors.rdsz;

import es.uc3m.it.webtlab.rdsz.decompressors.Decompressor;
import es.uc3m.it.webtlab.rdsz.utils.BufferDecompressor;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.DataFormatException;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class RDSZDecompressor implements Decompressor {
    
    int level;
    int cacheSize;
    String delimiter;          
    RDSZDiffDecoder decoder;
    BufferDecompressor decompressor;
    
    
    public RDSZDecompressor(String delimiter, int level, int cacheSize, int bufSize) {
        this.delimiter = delimiter;        
        this.cacheSize = cacheSize;
        this.decompressor = new BufferDecompressor(bufSize);        
        this.decoder = new RDSZDiffDecoder(cacheSize);
        this.level = level;        
    }

    @Override
    public List<String> decompress(byte[] data) throws IOException, DataFormatException {
        
        byte[] output = this.decompressor.decompress(data);
       
        // Convert the bytes to a String
        String itemsStr = new String(output, "UTF-8");                

        // Split the string into the RDF items using the delimiter
        String[] items = itemsStr.split(this.delimiter);
        
        LinkedList<String> decodedItems = new LinkedList<>();
        for (String item : items) {
            String decodedItem = this.decoder.decode(item);
            decodedItems.add(decodedItem);
        }

        return decodedItems;
    }
}
