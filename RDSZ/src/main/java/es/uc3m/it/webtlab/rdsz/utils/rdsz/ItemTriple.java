/*
 * Copyright (C) 2014 Norberto Fernandez <berto@it.uc3m.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uc3m.it.webtlab.rdsz.utils.rdsz;

import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import es.uc3m.it.webtlab.rdsz.utils.Constants;
import java.util.HashMap;

/**
 *
 * @author Norberto Fernandez <berto@it.uc3m.es>
 */
public class ItemTriple implements Comparable {

    private static final Model m = ModelFactory.createDefaultModel();
    private static final N3Formatter formatter = N3Formatter.getInstance();

    Node subject;
    Node predicate;
    Node object;

    Triple triple;
    String tripleString;
        
    public ItemTriple(Triple triple, HashMap<String, Integer> bnodeTable) {
        this.triple = triple;
        setSubject(triple.getSubject(), bnodeTable);
        setPredicate(triple.getPredicate(), bnodeTable);
        setObject(triple.getObject(), bnodeTable);
        setString();
    }

    public Node getSubject() {
        return this.subject;
    }

    public Node getPredicate() {
        return this.predicate;
    }

    public Node getObject() {
        return this.object;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof ItemTriple) {
            ItemTriple other = (ItemTriple)o;
            return this.toString().compareTo(other.toString());
        } else {
            throw new RuntimeException("Invalid object to compare");
        }
    }

    @Override
    public String toString() {
        return this.tripleString;
    }

    private void setSubject(com.hp.hpl.jena.graph.Node node, HashMap<String, Integer> bnodeTable) {

        Node subjectNode;
        int bnodeIdx = bnodeTable.size();
        if (node.isBlank()) {
            int idx;
            String sStr = node.getBlankNodeLabel();
            if (bnodeTable.containsKey(sStr)) {
                idx = bnodeTable.get(sStr);
            } else {
                idx = bnodeIdx;
                bnodeTable.put(sStr, idx);
                bnodeIdx++;
            }

            subjectNode = new Node("_:" + idx, Constants.NodeTypes.BNODE);

        } else if (node.isURI()) {
            subjectNode = new Node(formatter.formatURI(node.getURI()), Constants.NodeTypes.URI);
        } else {
            throw new RuntimeException("Unexpected node type for subject");
        }

        this.subject = subjectNode;
    }

    
    private void setPredicate(com.hp.hpl.jena.graph.Node node, HashMap<String, Integer> bnodeTable) {
        this.predicate = new Node(formatter.formatURI(node.getURI()), Constants.NodeTypes.URI);
    }

    
    private void setObject(com.hp.hpl.jena.graph.Node node, HashMap<String, Integer> bnodeTable) {

        Node objectNode;
        int bnodeIdx = bnodeTable.size();
        if (node.isBlank()) {
            int idx;
            String oStr = node.getBlankNodeLabel();
            if (bnodeTable.containsKey(oStr)) {
                idx = bnodeTable.get(oStr);
            } else {
                idx = bnodeIdx;
                bnodeTable.put(oStr, idx);
                bnodeIdx++;
            }

            objectNode = new Node("_:" + idx, Constants.NodeTypes.BNODE);

        } else if (node.isURI()) {
            objectNode = new Node(formatter.formatURI(node.getURI()), Constants.NodeTypes.URI);
        } else if (node.isLiteral()) {
            Literal l = (Literal) m.asRDFNode(node);
            objectNode = new Node(formatter.formatLiteral(l), Constants.NodeTypes.LITERAL);
        } else {
            throw new RuntimeException("Unexpected node type for object");
        }
        this.object = objectNode;
    }

    private void setString() {
        StringBuilder result = new StringBuilder();
        result.append(this.subject.text);
        result.append(" ");
        result.append(this.predicate.text);
        result.append(" ");
        result.append(this.object.text);
        result.append(" .\n");
        this.tripleString = result.toString();
    }

}
