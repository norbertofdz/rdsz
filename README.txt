Java implementation of the RDF stream compression algorithm RDSZ described in:

N. Fernández García, J. Arias-Fisteus, L. Sánchez, D. Fuentes-Lorenzo, O. Corcho. 
RDSZ: An Approach for Lossless RDF Stream Compression. 
The Semantic Web: Trends and Challenges 11th International Conference, ESWC, pp. 52-67 (2014).
DOI: http://dx.doi.org/10.1007/978-3-319-07443-6_5
